const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/whoami', (req, res) => {
  res.send('Yes, This is Case App!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})