#!/bin/bash

function init (){

  echo "================================================================================"
  echo "Setting AWS credentials..."
  echo "================================================================================"

  [[ $AWS_ACCESS_KEY_ID ]] ||{ access_key="echo \$$(echo "$ENVIRONMENT"| tr a-z A-Z)_ACCESS_KEY"; export AWS_ACCESS_KEY_ID=$(eval $access_key); }
  [[ $AWS_SECRET_ACCESS_KEY ]] || { secret_key="echo \$$(echo "$ENVIRONMENT"| tr a-z A-Z)_SECRET_KEY";export AWS_SECRET_ACCESS_KEY=$(eval $secret_key); }
  [[ $AWS_DEFAULT_REGION ]] || { region="echo \$$(echo "$ENVIRONMENT"| tr a-z A-Z)_REGION"; export AWS_DEFAULT_REGION=$(eval $region); }

  export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account)

}

function initTerraform(){

echo "Terraform Init."

state_bucket="$AWS_ACCOUNT_ID-$ENVIRONMENT-state"
service_state_folder="Case"

echo "================================================================================"
echo "All Following variables will be used in gitlab-ci.yml. Please check carefully..."
echo "================================================================================"
echo "ENVIRONMENT                : $ENVIRONMENT"
echo "ACCOUNT_ID                 : $AWS_ACCOUNT_ID"
echo "AWS_DEFAULT_REGION         : $AWS_DEFAULT_REGION"
echo "Service_state_folder       : $service_state_folder"
echo "IMAGE                      : $IMAGE"
echo "================================================================================"


export TF_VAR_environment=$ENVIRONMENT
export TF_VAR_image=$IMAGE

tfenv use 0.12.20
terraform init -backend-config="bucket=$state_bucket"  \
               -backend-config="key=$service_state_folder/app.state" \
               -backend-config="region=$AWS_DEFAULT_REGION"
}

function deploy(){
  initTerraform

  echo "Deploying App configurations to $ENVIRONMENT"

  terraform apply -auto-approve=true
}

function plan(){
  initTerraform

  echo "Planning to deploy App configurations to $ENVIRONMENT"

  terraform plan
}

function destroy(){
  initTerraform

  echo "Destroying App configurations for $ENVIRONMENT"

  terraform destroy --force
}

function main() {
  if [ "$1" == "plan" ];then
    init
    plan
  elif [ "$1" == "deploy" ];then
    init
    deploy
  elif [ "$1" == "destroy" ];then
    init
    destroy
  fi
}

set -e
main $1
