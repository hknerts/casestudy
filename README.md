# Case

I preferred to use ECS instead of EKS due to cost effects.

## Manual Actions

This resources are already exist, but we can create this resource via AWS CLI when during deployment.
Also Certificates can be provision via terraform.

* S3 State Buckets (798452604439-dev-state, 798452604439-prod-state)
* Certificates (caseapi.dev.hknerts.ml, caseapi.prod.hknerts.ml)

## Automated Actions

You can see all terraform code in repo.

* VPC (Public and Private Subnets, Internet and Nat Gateways, Security Groups)
* Load Balancer (Internet Facing ALB, Target Group)
* ECS Cluster
* Fargate Spot Service
* Route 53 Records

#### CI/CD stages
- Build (Builds applications and push the tag to registry.)
- Plan ( Terraform Plan - Scaffolding and App Infrastructure)
- Apply (Terraform Apply - Scaffolding and App Infrastructure)
- Destroy (Terraform Destroy - Scaffolding and App Infrastructure)

If the all steps are success, you can visit https://caseapi.{environment}.hknerts.ml

For example : https://caseapi.dev.hknerts.ml/whoami
