locals {
  name        = "CaseStudy-${var.environment}"
  environment = "${var.environment}"
  
  ec2_resources_name = "${local.name}"
}

variable "environment" {}

variable "image" {}
