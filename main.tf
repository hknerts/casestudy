module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = local.name

  cidr = "10.1.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  private_subnets = ["10.1.1.0/24", "10.1.2.0/24"]
  public_subnets  = ["10.1.11.0/24", "10.1.12.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    Environment = local.environment
    Name        = local.name
  }
}

module "ecs" {
  source = "terraform-aws-modules/ecs/aws"

  name               = local.name
  container_insights = false

  capacity_providers = ["FARGATE_SPOT"]

  default_capacity_provider_strategy = [{
    capacity_provider = "FARGATE_SPOT" # "FARGATE_SPOT"
    weight            = "1"
  }]

  tags = {
    Environment = local.environment
  }
}

module "app" {
  source              = "./service-app"
  environment         = var.environment
  cluster_id          = module.ecs.ecs_cluster_id
  security_groups     = [aws_security_group.ecs.id]
  private_subnets     = module.vpc.private_subnets
  vpc_id              = module.vpc.vpc_id
  alb_listener_arn    = aws_lb_listener.https_listener.arn
  image               = var.image
}

resource "aws_security_group" "alb" {
    name        = "CaseStudy-alb-sg-${var.environment}"
    description = "Allow TLS inbound traffic"
    vpc_id      = module.vpc.vpc_id
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
  }

}

resource "aws_security_group_rule" "https" {
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}


resource "aws_security_group" "ecs" {
    name        = "CaseStudy-ecs-sg-${var.environment}"
    description = "Allow TLS inbound traffic"
    vpc_id      = module.vpc.vpc_id
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
  }

}

resource "aws_security_group_rule" "service" {
  type            = "ingress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks     = [module.vpc.vpc_cidr_block]
  security_group_id = aws_security_group.ecs.id
}



resource "aws_lb" "alb" {
  name               = "CaseStudy-lb-${var.environment}"
  load_balancer_type = "application"
  internal           = false
  subnets            = module.vpc.public_subnets
  security_groups    = [aws_security_group.alb.id]

  enable_deletion_protection = false

}

data "aws_acm_certificate" "certificate" {
  domain = "caseapi.${var.environment}.hknerts.ml"
  statuses = ["ISSUED"]
}

resource "aws_lb_listener" "https_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   =  data.aws_acm_certificate.certificate.arn

  default_action {
    type             = "fixed-response"
    fixed_response {
      content_type = "text/html"
      message_body = "<!DOCTYPE html><html><body><h2>404 - Not Found</h2></body></html>"
      status_code = "404"
    }

  }
}

resource "aws_route53_record" "www" {
  zone_id = "Z03284601KY47TSRJ2VMI"
  name    = "caseapi.${var.environment}.hknerts.ml"
  type    = "A"

  alias {
    name                   = aws_lb.alb.dns_name
    zone_id                = aws_lb.alb.zone_id
    evaluate_target_health = true
  }
}