
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

resource "aws_cloudwatch_log_group" "case" {
  name              = "CaseStudy-${var.environment}"
  retention_in_days = 1
}
data "template_file" "task_definition_json" {
  template                                 = file("${path.module}/task_definition.json")
  vars = {
    environment                            = var.environment
    image                                  = var.image
  }
}

resource "aws_ecs_task_definition" "case" {
  family                    = "CaseStudy-${var.environment}"
  requires_compatibilities  = ["FARGATE"]
  network_mode              = "awsvpc"
  cpu                       = 512
  memory                    = 1024
  execution_role_arn        = aws_iam_role.task_execution_role.arn
  task_role_arn             = aws_iam_role.task_role.arn
  container_definitions     = data.template_file.task_definition_json.rendered
}

resource "aws_iam_role" "task_execution_role" {
  name                = "case-${var.environment}-task_execution_role"
  assume_role_policy  = file("${path.module}/policies/task_execution_role.json")
}


resource "aws_iam_role" "task_role" {
  name                = "case-${var.environment}-task-role"
  assume_role_policy  = file("${path.module}/policies/task_role.json")

}

resource "aws_iam_role_policy" "task_policy" {
  name    = "case-${var.environment}-task-policy"
  role    = aws_iam_role.task_role.id
  policy  = file("${path.module}/policies/task_policy.json")
}

resource "aws_iam_role_policy" "task_execution_policy" {
  name    = "case-${var.environment}-task_execution_policy"
  role    = aws_iam_role.task_execution_role.id
  policy  = data.template_file.task_execution_policy_file.rendered
  depends_on = [data.template_file.task_execution_policy_file]
}

data "template_file" "task_execution_policy_file" {
  template = file("${path.module}/policies/task_execution_policy.json")
  vars = {
    region      = data.aws_region.current.name
    account_id  = data.aws_caller_identity.current.account_id
  }
}


resource "aws_ecs_service" "casestudy" {
  launch_type     = "FARGATE"
  name            = "Case1-${var.environment}"
  cluster         = var.cluster_id
  task_definition = aws_ecs_task_definition.case.arn

  desired_count = 1

  network_configuration {
    security_groups = var.security_groups
    subnets         = var.private_subnets
    assign_public_ip = false
  }

  load_balancer {
    container_name   = "Case1-${var.environment}"
    container_port   = 3000
    target_group_arn = aws_alb_target_group.service_alb_target_group.arn
  }

  depends_on = [aws_alb_target_group.service_alb_target_group]
}

resource "aws_alb_target_group" "service_alb_target_group" {
  name      = "Case1-TG-${var.environment}"
  port      = 3000
  protocol  = "HTTP"
  vpc_id    = var.vpc_id
  target_type = "ip"

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
  }
}

resource "aws_alb_listener_rule" "alb_rule" {
  listener_arn = var.alb_listener_arn

  action {
    order = 1
    type = "forward"
    target_group_arn = aws_alb_target_group.service_alb_target_group.arn
  }


  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  depends_on = [aws_alb_target_group.service_alb_target_group]
}

