variable "cluster_id" {
  description = "The ECS cluster ID"
  type        = string
}

variable "environment" {}
variable "security_groups" {}
variable "private_subnets" {}
variable "vpc_id" {}
variable "alb_listener_arn" {}
variable "image" {}